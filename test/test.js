var assert = require('assert');
var WorkPlanner = require('./../workplanner.js');

describe('Valid Test Cases', function () {
    describe('Passing a single task', function () {
        it('should return A', function () {
            var tasks = { "A": "" };
            result = WorkPlanner(tasks);
            assert.equal(result, "A");
        });
    });


    describe('Passing empty string', function () {
        it('should return an empty string', function () {
            var tasks = "";
            result = WorkPlanner(tasks);
            assert.equal(result, "");
        });
    });

    describe('Passing tasks with dependencies', function () {
        it('should return A,F,C,B,D,E', function () {
            var tasks = {
                "A": "",
                "B": "C",
                "C": "F",
                "D": "A",
                "E": "B",
                "F": "",
            };
            result = WorkPlanner(tasks);
            assert.equal(result, "A,F,C,B,D,E");
        });
    });

});

describe('Invalid Test Cases', function () {
    describe('Task depends on itself', function () {
        it('should return Error', function () {
            var tasks = {
                "A": "A"
            };
            assert.throws(WorkPlanner.bind(this, tasks), Error)
        });
    });

    describe('Tasks with circular reference', function () {
        it('should return Error', function () {
            var tasks = {
                "A": "",
                "B": "C",
                "C": "F",
                "D": "A",
                "E": "",
                "F": "B"
            };
            assert.throws(WorkPlanner.bind(this, tasks), Error)
        });
    });
});
