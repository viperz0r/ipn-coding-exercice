var _ = require("underscore");

function WorkPlanner(tasks) {
    // when no argument is provided, we initialize as an empty object
    tasks = (typeof tasks === "undefined" || tasks === "") ? {} : tasks;

    checkIfTasksAreValid();
    var orderedList = [];
    var firstLevelTasks = Object.keys(tasks);

    return solveTaskDependencies();

    /**
     * Process tasks and return the correct task order
     * @return {string} 
     */
    function solveTaskDependencies() {
        // iterate all first level keys
        for (i = 0; i < firstLevelTasks.length; i++) {
            var task = firstLevelTasks[i];
            if (isAlreadyCalculated(task))
                continue;

            processDependency(task);
        }

        return orderedList.toString();
    }

    /**
     * Process task dependency recursively until find a final state.
     * Then add that
     * @param {string} task 
     * @param {string} dependencyHistory 
     */
    function processDependency(task, dependencyHistory) {
        var taskDependency = tasks[task];
        dependencyHistory = dependencyHistory || "";

        checkIfDependencyIsValid(task, taskDependency);
        checkCircularReference(task, dependencyHistory);
        dependencyHistory += task;

        if (isFinalState(taskDependency) || isAlreadyCalculated(taskDependency)) {
            orderedList.push(task);
        } else {
            processDependency(taskDependency, dependencyHistory);
            orderedList.push(task);
        }
    }

    /**
     * Check if the task is a final state ( without dependencies )
     * @param {string} element 
     */
    function isFinalState(element) {
        return element === "";
    }

    /**
     * Check if a task was already calculated
     * @param {string} element 
     */
    function isAlreadyCalculated(element) {
        return orderedList.indexOf(element) > -1;
    }

    /**
     * Check for circular reference
     * @param {string} task 
     * @param {string} dependencyHistory 
     */
    function checkCircularReference(task, dependencyHistory) {
        if (dependencyHistory.indexOf(task) > -1) {
            throw Error("Circular Reference");
        }
        return false;
    }

    /**
     * Validate task and task dependency
     * @param {string} task 
     * @param {string} taskDependency 
     */
    function checkIfDependencyIsValid(task, taskDependency) {
        // test is task and dependency are equal
        if (task === taskDependency) {
            throw Error("A task cannot be dependent on itself");
        }

        // check if task dependency exists as a task
        if (firstLevelTasks.indexOf(taskDependency) === -1 && taskDependency !== "") {
            throw Error(`Task dependency ${taskDependency} doesn´t exist as a Task`);
        }
    }

    /**
     * Basic test to argument provided
     */
    function checkIfTasksAreValid() {
        if (!_.isObject(tasks)) {
            throw Error("Invalid Tasks Object - Should be an Object");
        }

    }

}

module.exports = WorkPlanner;


