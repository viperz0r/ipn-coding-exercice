# README #

# Dependências
* NodeJs 6+
* NPM

# Instalação
npm install

# Iniciar o programa
npm start

# Executar testes unitários
npm test

# Dúvidas na análise do problema
No início (Dica 1) pensei que seria uma função que simplesmente receberia uma string com as tarefas a executar e não a sua estrutura de dependências.
Contudo através dos exemplos dados, concluí que o pretendido seria passar a estrutura de tarefas e respetivas dependências

# Análise do problema
No principio tentei uma abordagem mais iterativa em que iria ordenando os elementos segundo as suas dependências.
Rapidamente percebi que esta solução iria ficar complexa e com pouca eficiência. 
Procurei imaginar as tarefas como grafos que teriam dependências e aí o problema ficou mais simples.
O objetivo foi criar uma função que determinasse recursivamente as dependências de uma tarefa até chegar a um estado final. 
Durante todo o processo teria que guardar as tarefas já calculadas para não as processar novamente.